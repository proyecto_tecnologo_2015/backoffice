'use strict';

/**
 * Servicios genericos para toda la app.
 */
app.service('commonService', ['$rootScope', '$http', '$q',

    function($rootScope, $http, $q){

        this.getPaises = function () {
            var defer = $q.defer();
            var url = $rootScope.SSEM_BACKEND_ENDPOINT + 'common/paises/lista';
            $http.get(url)
                .then(
                function success(response){
                    defer.resolve(response);
                },
                function error(error, status){
                    defer.reject(error);
                }
            );
            return defer.promise;
        };

        this.getDatosOperador = function(idOperador) {
            var defer = $q.defer();
            var url = $rootScope.SSEM_BACKEND_COMMON+'mostrar/verOperador';
            $http.get(url, {params: {idOperador: idOperador}})
                .then(
                function success(response){
                    defer.resolve(response.data);
                },
                function error(error){
                    defer.reject(error);
                }
            );
            return defer.promise;
        };


    }]);