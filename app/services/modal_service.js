'use strict';
app.factory('modalService', ['$uibModal', function($uibModal) {
    var errorModal = function(msg) {
        return $uibModal.open({
            templateUrl: 'views/modals/error.html',
            controller: function($uibModalInstance, $scope) {
                if(msg) {
                    $scope.msg = msg;
                }
                $scope.close = function() {
                    $uibModalInstance.close();
                }
            }
        });
    };

    var successModal = function(msg) {
        return $uibModal.open({
            animation: true,
            templateUrl: 'views/modals/success.html',
            windowClass: 'center-modal',
            controller: function($uibModalInstance, $scope) {
                if(msg) {
                    $scope.msg = msg;
                }
                $scope.close = function() {
                    $uibModalInstance.close();
                }
            }
        });
    };

    return {
        errorModal : errorModal,
        successModal: successModal
    }
}]);
