'use strict';
app.service('novedadService', ['$rootScope', '$http', '$q',
    function ($rootScope, $http, $q) {

        this.altaNovedad = function (evento) {
            var defer = $q.defer();
            var url = $rootScope.SSEM_BACKEND_ENDPOINT + 'novedades/altaNovedad';
            $http.post(url, evento)
                .then(
                function success(response) {
                    defer.resolve(response);
                },
                function error(error, status) {
                    defer.reject(error);
                }
            );
            return defer.promise;
        };
        this.getNovedadesEvento = function (idEvento) {
            var defer = $q.defer();
            var url = $rootScope.SSEM_BACKEND_COMMON + 'mostrar/verNovedadesEvento';
            $http.get(url, {params: {idEvento: idEvento}})
                .then(
                function success(response) {
                    defer.resolve(response.data);
                },
                function error(error) {
                    defer.reject(error);
                }
            );
            return defer.promise;
        };

        this.getDetalleNovedad = function(idNovedad) {
            var defer = $q.defer();
            var url = $rootScope.SSEM_BACKEND_COMMON + 'mostrar/detalleNovedad';
            $http.get(url, {params: {idNovedad : idNovedad}})
                .then(
                    function success(response){
                        defer.resolve(response.data);
                    },
                    function error(error){
                        defer.reject(error);
                    }
                );
            return defer.promise;
        };

        return this;
    }]);
