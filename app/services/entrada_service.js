/**
 * Created by dalepo on 12/05/16.
 */
app.service('entradaService', ['$http', '$q', '$rootScope', function($http, $q, $rootScope) {

    this.altaEntradas = function (entradas) {
        var defer = $q.defer();
        var url = $rootScope.SSEM_BACKEND_ENDPOINT+ 'encuentro/altaEntrada';
        $http.post(url, entradas).then(
            function success() {
                defer.resolve();
            },
            function error(error) {
                defer.reject(error);
            }
        );
        return defer.promise;
    };

    this.getEntradasEncuentro = function (idEncuentro) {
        var defer = $q.defer();
        var url = $rootScope.SSEM_BACKEND_COMMON + 'mostrar/verEntradas';
        $http.get(url, {params:{idEncuentro: idEncuentro}}).then(
            function success(response) {
                defer.resolve(response.data);
            },
            function error(error) {
                defer.reject(error);
            }
        );
        return defer.promise;
    };



}]);