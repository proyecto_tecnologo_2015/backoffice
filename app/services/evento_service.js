'use strict';

app.service('eventoService', ['$rootScope', '$http', '$q',
    function($rootScope, $http, $q){

        this.editarEvento = function(evento) {
            var defer = $q.defer();
            var url = $rootScope.SSEM_BACKEND_ENDPOINT + 'eventos/editarEvento';
            $http.post(url, evento)
                .then(
                function success(response){
                    defer.resolve(response.data);
                },
                function error(error){
                    defer.reject(error);
                }
            );
            return defer.promise;
        };

        this.crearEvento = function(evento) {
            var defer = $q.defer();
            var url = $rootScope.SSEM_BACKEND_ENDPOINT + 'eventos/altaEventos';
            $http.post(url, evento)
                .then(
                function success(response){
                    defer.resolve(response);
                },
                function error(error){
                    defer.reject(error);
                }
            );
            return defer.promise;
        };

        this.crearDisciplina = function(disciplina) {
            var defer = $q.defer();
            var url =  $rootScope.SSEM_BACKEND_ENDPOINT + 'eventos/crearDisciplina';
            $http.post(url, disciplina)
                .then(
                function success(response){
                    defer.resolve(response);
                },
                function error(error){
                    defer.reject(error);
                }
            );
            return defer.promise;
        };

        this.getDatosEvento = function(idEvento) {
            var defer = $q.defer();
            var url = $rootScope.SSEM_BACKEND_COMMON + 'mostrar/getEvento';
            $http.get(url, {params: {idEvento: idEvento}})
                .then(
                function success(response){
                    defer.resolve(response.data);
                },
                function error(error){
                    defer.reject(error);
                }
            );
            return defer.promise;
        };

        this.getDisciplinasEvento = function(idEvento) {
            var defer = $q.defer();
            var url = $rootScope.SSEM_BACKEND_ENDPOINT + 'eventos/verDisciplinasEvento';
            $http.get(url, {params: {idEvento: idEvento}})
                .then(
                    function success(response){
                        defer.resolve(response.data);
                    },
                    function error(error){
                        defer.reject(error);
                    }
                );
            return defer.promise;
        };


    }]);
