/**
 * Created by dalepo on 15/05/16.
 */
app.service('reporteService', ['$http', '$q', '$rootScope', function($http, $q, $rootScope){

    this.getReporteEntradasEncuentro = function(request) {
        var defer = $q.defer();
        var url = $rootScope.SSEM_BACKEND_ENDPOINT + 'encuentro/reporteEntrada';
        $http.post(url, request).then(
            function success(response) {
                defer.resolve(response.data);
            },
            function error(error) {
                defer.reject(error);
            }
        );
        return defer.promise;
    };

}]);