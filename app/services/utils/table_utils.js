app.factory('tableUtils', [function()
{

    var newDefaultTable = function(scope, columns, data) {
        scope.table.gridOptions = {
            data: data,
            columnDefs: columns,
            multiSelect: false,
            noUnselect: false,
            modifierKeysToMultiSelect: false,
            enableRowSelection: true,
            enableRowHeaderSelection: false
        };
        scope.table.gridOptions.onRegisterApi = function( gridApi ) {
            scope.gridApi = gridApi;
        };

    };

    var columnHelper = {
        newEditColumn : function(clickFunction) {
            return {
                name: 'Editar',
                cellTemplate: '<a href="" ng-click="grid.appScope.'+clickFunction+'(row.entity)"><i class="glyphicon glyphicon-edit"></i></a>',
                cellClass: 'text-center',
                sortable: false,
                width: 70
            }
        },
        newDetailColumn : function (clickFunction) {
            return {
                name: 'Detalle',
                cellTemplate: '<a href="" ng-click="grid.appScope.'+clickFunction+'(row.entity)"><i class="fa fa-search"></i></a>',
                cellClass: 'text-center',
                sortable: false,
                width: 70
            }
        },
        newEditDeleteColumn: function() {
            return {
                name: ' ',
                cellTemplate: '<a href="" class="ssem-grid-button" ng-click="grid.appScope.edit(row.entity)"><i class="fa fa-pencil"></i></a>'
                                +'<a href="" class="ssem-grid-button" ng-click="grid.appScope.delete(row.entity)"><i class="fa fa-trash-o"></i></a>',
                cellClass: 'text-center',
                sortable: false,
                enableColumnMenu: false,
                width: 90
            }
        },
        newEditDeleteDetailColumn: function() {
            return {
                name: ' ',
                cellTemplate:
                '<a href="" class="ssem-grid-button" ng-click="grid.appScope.edit(row.entity)"><i class="fa fa-pencil"></i></a>'
                +'<a href="" class="ssem-grid-button" ng-click="grid.appScope.delete(row.entity)"><i class="fa fa-trash-o"></i></a>'
                + '<a href="" class="ssem-grid-button" ng-click="grid.appScope.detail(row.entity)"><i class="fa fa-search"></i></a>',
                cellClass: 'text-center',
                sortable: false,
                enableColumnMenu: false,
                width: 130
            }
        },
        newDateColumn: function(field, columnName, format) {
            return {
                name: columnName,
                field: field,
                sortable: true,
                cellFilter: 'date:"'+format + '"',
                width: 150
            }
        }

    };

    return {
        newDefaultTable : newDefaultTable,
        columnHelper: columnHelper

    }

}]);