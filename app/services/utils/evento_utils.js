/**
 * Created by dalepo on 16/05/16.
 */
app.factory('eventoUtils', [function() {

    var isEventoFinalizado = function (evento) {
        var fechaFin = new Date(evento.fechaFin);
        if (!fechaFin)
            return false;
        var now = new Date();
        return now.getTime() > fechaFin.getTime();
    };


    return {
        isEventoFinalizado : isEventoFinalizado
    };

}]);