app.factory('commonUtils', [ '$rootScope', function($rootScope){

    var converDateToArray = function(date) {
        return [
            date.getFullYear(), date.getMonth() + 1, date.getDate(), date.getHours(), date.getMinutes()
        ];
    };

    var buildUrlEvento = function(evento) {
        return ($rootScope.sslConfig ? 'https' : 'http') + '://services.ssem.org/web/eventos/multideportivos/' + evento.idEvento;
    };

    //Chanchisimo, pero el formato que mandan en el backend es un asco. Se deberia usar el formato ISO estandar.
    var parseDateFromArray = function(array) {

        if(!array || !array.length || array.length == 0)
            return '';

        var date = new Date();
        date.setFullYear(array[0]);
        date.setMonth(array[1] - 1);
        date.setDate(array[2]);

        if(array.length > 4) {
            date.setHours(array[3]);
            date.setMinutes(array[4]);
        }
        return date;
    };

    var isUrlForBackend = function(url) {
        return angular.isString(url) && url.indexOf($rootScope.SSEM_BACKEND_REST_DEF) !== -1;
    };
    var isNotUrlForLogin = function(url){
        return url.indexOf('loginFirstStep') === -1 || url.indexOf('tryLogin') === -1;
    };
    var esOrigenRepetido = function (origenes, origen) {
        if(!origenes)
            return false;
        for(var i=0; i<origenes.length ; i++) {
            var current = origenes[i];
            if(current.tipoOrigen == origen.tipoOrigen) {
                origen.id = current.id;
                if(current.urlApi == origen.urlApi)
                    return true;
            }
        }
        return false;
    };
    return {
        converDateToArray: converDateToArray,
        parseDateFromArray: parseDateFromArray,
        isUrlForBackend: isUrlForBackend,
        isNotUrlForLogin: isNotUrlForLogin,
        esOrigenRepetido: esOrigenRepetido,
        buildUrlEvento: buildUrlEvento
    }
}]);
