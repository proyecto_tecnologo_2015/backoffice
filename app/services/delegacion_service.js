app.service('delegacionService', ['$http', '$q', '$rootScope', function($http, $q, $rootScope){

    this.getDetalleEventoDelegacion = function() {

        var defer = $q.defer();
        var url = $rootScope.SSEM_BACKEND_ENDPOINT + 'eventos/detalleEventoDelegacion';
        $http.get(url)
            .then(
            function success(response){
                if(angular.isArray(response.data)) {
                    defer.resolve(response.data[0]);
                }
                else {
                    defer.reject(error);
                }
            },
            function error(error){
                defer.reject(error);
            }
        );
        return defer.promise;
    };

}]);