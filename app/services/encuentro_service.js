app.service('encuentroService', ['$http', '$q', '$rootScope', function($http, $q, $rootScope){

    this.altaEncuentro = function(encuentro) {
        var defer = $q.defer();
        var url = $rootScope.SSEM_BACKEND_ENDPOINT + 'encuentro/altaEncuentro';
        $http.post(url, encuentro).then(
            function success() {
                defer.resolve();
            },
            function error(error) {
                defer.reject(error);
            }
        );
        return defer.promise;
    };

    this.getEncuentrosEvento = function(idEvento) {
        var defer = $q.defer();
        var url = $rootScope.SSEM_BACKEND_COMMON + 'mostrar/getEncuentroEvento';
        $http.get(url, {params: {idEvento: idEvento}}).then(
            function success(response) {
                defer.resolve(response.data);
            },
            function error(error) {
                defer.reject(error);
            }
        );
        return defer.promise;
    };

    this.getDetalleEncuentro = function(idEvento) {
        var defer = $q.defer();
        var url = $rootScope.SSEM_BACKEND_COMMON + 'mostrar/verEncuentro';
        $http.get(url, {params: {idEncuentro: idEvento}}).then(
            function success(response) {
                defer.resolve(response.data);
            },
            function error(error) {
                defer.reject(error);
            }
        );
        return defer.promise;
    }

}]);
