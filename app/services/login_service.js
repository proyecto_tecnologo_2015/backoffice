'use strict';

/**
 * Generico para todos los usuarios de backoffice
 */
app.service('loginService', ['$rootScope', '$http', '$q', 'authService',
    function ($rootScope, $http, $q, authService) {


        this.doLogin = function (user) {

            var defer = $q.defer();

            var urlFirstStep = $rootScope.SSEM_BACKEND_SECURITY_ENDPOINT + 'operador/loginFirstStep';
            var urlSecondStep = $rootScope.SSEM_BACKEND_SECURITY_ENDPOINT + 'operador/tryLogin';

            var userHeader = {
                email: user.userName
            };
            userHeader = angular.toJson(userHeader);
            var request = {
                method: 'POST',
                url: urlFirstStep,
                headers: {
                    'Content-Type': 'application/json',
                    '_ssem_user_header': btoa(userHeader)
                }
            };

            $http(request).then(
                function success(response) {
                    var usuarioFirstResponse = response.data.response;
                    //Si respondio bien el objeto es que hubo exito en el primer paso
                    if (angular.isObject(usuarioFirstResponse)) {
                        //Seteamos la pass
                        usuarioFirstResponse.password = user.password;
                        var jsonUsuario = angular.toJson(usuarioFirstResponse);
                        //Cambiamos la url y el user del header
                        request.url = urlSecondStep;
                        request.headers._ssem_user_header = btoa(jsonUsuario);
                        $http(request).then(
                            function success(secondResponse) {
                                var user = secondResponse.data.response;
                                authService.storeUser(user)
                                if(!authService.userIsOrganizador() && !authService.userIsDelegacion()) {
                                    authService.removeUserCookie();
                                    defer.reject('El operador no existe!');
                                }
                                defer.resolve(secondResponse);
                            },
                            function (error) {
                                defer.reject(error);
                            }
                        )
                    }

                },
                function error(error) {
                    defer.reject(error);
                }
            );
            return defer.promise;
        }
    }]);
