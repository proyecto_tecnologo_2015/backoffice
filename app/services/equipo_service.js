app.service('equipoService', ['$http', '$q', '$rootScope', function($http, $q, $rootScope){

    this.getEquiposEvento = function(idEvento) {

        var defer = $q.defer();
        var url = $rootScope.SSEM_BACKEND_COMMON + 'mostrar/listarEquipos';
        $http.get(url, {params: {idEvento : idEvento}})
            .then(
            function success(response){
                defer.resolve(response.data);
            },
            function error(error){
                defer.reject(error);
            }
        );
        return defer.promise;

    };

    this.altaEquipo = function(equipo) {

        var defer = $q.defer();
        var url = $rootScope.SSEM_BACKEND_ENDPOINT+ 'encuentro/altaEquipo';
        $http.post(url, equipo)
            .then(
            function success(response){
                defer.resolve(response.data);
            },
            function error(error){
                defer.reject(error);
            }
        );
        return defer.promise;
    };

    this.altaDeportista = function(deportista) {

        var defer = $q.defer();
        var url = $rootScope.SSEM_BACKEND_ENDPOINT+ 'delegacion/altaDeportista';
        var request = {};
        //Armo la lista de equipos para que mapee correctamente
        var equipoId = deportista.equipos;
        deportista.equipos = [];
        deportista.equipos.push({'id' : equipoId});
        request.request = deportista;
        $http.post(url, request)
            .then(
            function success(response){
                defer.resolve(response.data);
            },
            function error(error){
                defer.reject(error);
            }
        );
        return defer.promise;
    };

    this.getDeportistasEventoOrganizador = function (idEvento) {
        var defer = $q.defer();
        var url = $rootScope.SSEM_BACKEND_COMMON+ 'mostrar/EventoDeportistas';
        $http.get(url, {params: {idEvento: idEvento}})
            .then(
                function success(response){
                    defer.resolve(response.data);
                },
                function error(error){
                    defer.reject(error);
                }
            );
        return defer.promise;
    };

    this.getDeportistasEvento = function(idEvento) {

        var defer = $q.defer();
        var url = $rootScope.SSEM_BACKEND_COMMON+ 'mostrar/listarDeportisas';
        $http.get(url, {params: {idEvento: idEvento}})
            .then(
            function success(response){
                defer.resolve(response.data);
            },
            function error(error){
                defer.reject(error);
            }
        );
        return defer.promise;
    };

    this.getEquiposDelegacion = function() {

        var defer = $q.defer();
        var url = $rootScope.SSEM_BACKEND_ENDPOINT+ 'operadores/verEquiposDelegacion';
        $http.post(url)
            .then(
                function success(response){
                    defer.resolve(response.data.response);
                },
                function error(error){
                    defer.reject(error);
                }
            );
        return defer.promise;
    };

    this.getDetalleDeportista = function(idDeportista) {

        var defer = $q.defer();
        var url = $rootScope.SSEM_BACKEND_COMMON + 'mostrar/verDeportista';
        $http.get(url, {params: {idDeportista : idDeportista}})
            .then(
                function success(response){
                    defer.resolve(response.data);
                },
                function error(error){
                    defer.reject(error);
                }
            );
        return defer.promise;
    };

    this.getDetalleEquipo = function(idEquipo) {
        var defer = $q.defer();
        var url = $rootScope.SSEM_BACKEND_COMMON + 'mostrar/verEquipo';
        $http.get(url, {params: {idEquipo : idEquipo}})
            .then(
                function success(response){
                    defer.resolve(response.data);
                },
                function error(error){
                    defer.reject(error);
                }
            );
        return defer.promise;
    };

    this.getListaEquipos = function(idEvento) {
        var defer = $q.defer();
        var url = $rootScope.SSEM_BACKEND_COMMON + 'mostrar/listarEquipos3D';
        $http.get(url, {params: {idEvento : idEvento}})
            .then(
                function success(response){
                    defer.resolve(response.data);
                },
                function error(error){
                    defer.reject(error);
                }
            );
        return defer.promise;
    };

}]);