app.service('authService', ['$cookies', '$state', '$rootScope', '$log', function($cookies, $state, $rootScope, $log){

    var USER_STORE_LOCATION = 'SSEM_LOGGED_USER';
    var USER_HEADER_STORE_LOCATION = 'SSEM_USER_HEADER';

    var _PERFIL_MOD = 'MODERADOR';
    var _PERFIL_ORG = 'ORGANIZADOR';
    var _PERFIL_DEL = 'DELEGACION';

    var storeUser = function(user) {
        setUser(user);
        $rootScope.$broadcast('#logeoEvent');
    };

    var setUser = function(user) {
        delete user.imagen;
        $log.debug('********* Se logeo ' + user.email + ' *********');
        $log.debug(angular.toJson(user, true));
        $log.debug('********* Logging *********');
        $cookies.putObject(USER_STORE_LOCATION, user);
    };

    var authorize = function(perfil) {
        var user = getUser();
        if(user) {
            return user.perfil.nombre === perfil;
        }
        return false;
    };


    var getUser = function() {
        return $cookies.getObject(USER_STORE_LOCATION);
    };

    var userIsModerador = function() {
        var user = getUser();
        return user.perfil.nombre === _PERFIL_MOD;
    };

    var userIsDelegacion = function() {
        var user = getUser();
        return user.perfil.nombre === _PERFIL_DEL;
    };

    var userIsOrganizador = function() {
        var user = getUser();
        return user.perfil.nombre === _PERFIL_ORG;
    };

    var isAuthenticated = function() {
        var user = getUser();
        return user !== null && typeof user  == 'object';
    };

    var logout = function() {
        removeUserCookie();
        $rootScope.$broadcast('#logeoEvent');
    };

    var removeUserCookie = function () {
        $cookies.remove(USER_STORE_LOCATION);
        $cookies.remove(USER_HEADER_STORE_LOCATION);
    };

    var getUserId = function() {
        var user = getUser();
        return user.id;
    };

    var redirectUserStart = function() {
        //Si no esta haciendo ninguna transicion y esta autenticado..
        if(isAuthenticated() && $state.transition == null) {
            if(userIsOrganizador()) {
                $state.go('home');
            }
            else if(userIsModerador()) {
                $state.go('home');//Por ahora..
            }
            else if(userIsDelegacion()) {
                $state.go('homeDelegacion').then();
            }
            else {
                logout();
                $state.go('index');
            }
        }
    };
    /**
     * Construye el header en base a los datos del cookie.
     * Lo parsea a json y luego b64
     * @returns {*|string}
     */
    var getHeader = function() {
        return $cookies.get(USER_HEADER_STORE_LOCATION);
    };

    var storeUserHeader = function(headerStr) {
        $cookies.put(USER_HEADER_STORE_LOCATION, headerStr);
    };

    return {
        userIsModerador: userIsModerador,
        userIsDelegacion: userIsDelegacion,
        userIsOrganizador: userIsOrganizador,
        isAuthenticated: isAuthenticated,
        getUser: getUser,
        storeUser: storeUser,
        authorize: authorize,
        logout: logout,
        removeUserCookie : removeUserCookie,
        redirectUserStart: redirectUserStart,
        getHeader: getHeader,
        getUserId: getUserId,
        storeUserHeader: storeUserHeader
    }

}]);
