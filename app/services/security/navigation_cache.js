app.factory('navigationCache', ['$cookies', 'commonUtils', function($cookies, commonUtils){

    var _selected_evento = 'SSEM_SELECTED_EVENTO';
    var _evento_delegacion = 'SSEM_EVENTO_DELEGACION';
    var _datosEvento = 'SSEM_DATOS_EVENTO';
    var _cacheindexes = [];

    var memoryCache = function() {

        var self = {};
        self.map = {};

        self.put = function(key, object) {
            self.map[key] = object;
        };
        self.get = function(key) {
            return self.map[key];
        };
        self.remove = function(key) {
            delete self.map[key];
        };
        self.removeAll = function(){
            self.map = {};
        };
        return self;
    };



    _cacheindexes.push(_selected_evento);



    var cleanCache = function() {
        _cacheindexes.forEach(function(val) {
            $cookies.remove(val);
        });
    };

    var setSelectedEvento = function(idEvento) {
        $cookies.put(_selected_evento, idEvento);
    };

    var setDatosEvento = function(evento) {
        var datosEvento = {
            descripcion: evento.descripcion,
            disciplinas: evento.disciplinas,
            estado: evento.estado,
            fechaFin: commonUtils.parseDateFromArray(evento.fechaFin),
            fechaInicio: commonUtils.parseDateFromArray(evento.fechaInicio),
            idEvento: evento.idEvento,
            nombre: evento.nombre,
            origenes: evento.origenes,
            pais: evento.pais
        };
        $cookies.putObject(_datosEvento, datosEvento);
    };

    var getDatosEvento = function () {
        return $cookies.getObject(_datosEvento);
    };

    var getSelectedEvento = function() {
        return $cookies.get(_selected_evento);
    };

    var getEventoDelegacion = function() {
        return $cookies.getObject(_evento_delegacion);
    };

    var setEventoDelegacion = function(evento) {
        delete evento.imagenes;
        $cookies.putObject(_evento_delegacion, evento);
    };

    var get = function(key) {
        return $cookies.getObject(key);
    };

    var put = function(key, value) {
        return $cookies.putObject(key, value);
    };

    return {
        cleanCache: cleanCache,
        setSelectedEvento: setSelectedEvento,
        getSelectedEvento: getSelectedEvento,
        getEventoDelegacion: getEventoDelegacion,
        setEventoDelegacion: setEventoDelegacion,
        setDatosEvento : setDatosEvento,
        getDatosEvento : getDatosEvento,
        memoryCache: memoryCache(),
        get: get,
        put: put
    }
}]);