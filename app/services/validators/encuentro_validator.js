app.factory('encuentroValidator', ['navigationCache', '$filter', '$rootScope', function(navigationCache, $filter, $rootScope){

    var validateStepOne = function(encuentro) {
        var errors = [];
        if(!encuentro) {
            errors.push('Error inesperado.');
            return errors;
        }
        if(! encuentro.fecha) {
            errors.push('Formato de fecha de inicio incorrecto.');
            return errors;
        }
        if(!encuentro.fechafin) {
            errors.push('Formato de fecha fin incorrecto.');
            return errors;
        }
        if(!encuentro.idDisciplina) {
            errors.push('Debe seleccionar al menos una disciplina.');
        }
        var datosEvento = navigationCache.getDatosEvento();
        validateDatesInRangeOfEvento(encuentro.fecha, encuentro.fechafin, datosEvento, errors);
        return errors;
    };

    var validateStepTwo = function(encuentro) {
        var errors = [];
        if(encuentro.equipos.length < 1) {
            errors.push('Debe seleccionar por lo menos 1 equipo.');
        }
        return errors;
    };

    var validateDatesInRangeOfEvento = function (fechaDesde, fechaHasta, datosEvento, errors) {

        var encuentroDesde = new Date(fechaDesde);
        var encuentroHasta = new Date(fechaHasta);
        var eventoDesde = new Date(datosEvento.fechaInicio);
        var eventoHasta = new Date(datosEvento.fechaFin);
        if(encuentroDesde.getTime() > encuentroHasta.getTime()) {
            errors.push('Fecha de inicio no puede ser mayor a la fecha fin.');
        }
        if(encuentroDesde.getTime() < eventoDesde.getTime()) {
            errors.push('Fecha de inicio no puede ser anterior a la del evento ('+ $filter('date')(eventoDesde, $rootScope.dateTimeFormat)+').');
        }
        else if(encuentroHasta.getTime() > eventoHasta.getTime()) {
            errors.push('Fecha fin no puede ser posterior a la del evento ('+$filter('date')(eventoHasta, $rootScope.dateTimeFormat)+ ').')
        }
    };

    var terminoEncuentro = function (encuentro) {
        if(!encuentro.fechafin)
            return false;
        var fechaFin = new Date(encuentro.fechafin);
        return new Date().getTime() > fechaFin.getTime();
    };

    return {
        validateStepOne : validateStepOne,
        validateStepTwo: validateStepTwo,
        terminoEncuentro : terminoEncuentro,
        validateDatesInRangeOfEvento : validateDatesInRangeOfEvento
    }
}]);