angular.module('AppInterceptors', [])

/**
 *  Interceptor para los request de $http
 */
    .factory('dateFormatInterceptor', ['$rootScope', function ($rootScope) {

        function convertDateToArray(date) {
            return [
                date.getFullYear(), date.getMonth() + 1, date.getDate(), date.getHours(), date.getMinutes()
            ];
        }

        /**
         * Convierte TODAS las fechas a fecha-array de cualquier objeto, recorre todos los atributos y si hay
         * un tipo Date, entonces lo convierte.
         * Lo hace recursivamente, por ejemplo si dentro de un objeto hay otro objeto, seguira recursivamente
         * buscando por tipo Date.
         * @param request
         *
         * @returns {*}
         */
        function convertAllDates(request) {
            if (typeof request !== 'object')
                return request;
            for (var key in request) {
                if (!request.hasOwnProperty(key))
                    continue;
                var value = request[key];
                if (value instanceof Date) {
                    request[key] = convertDateToArray(value);
                }
                else if (typeof value === 'object') {
                    //llamada recursiva
                    convertAllDates(value);
                }
            }
        }

        return {
            request: function (config) {
                if (config.url && config.url.indexOf($rootScope.SSEM_BACKEND_REST_DEF) !== -1) {
                    if (config.data) {
                        convertAllDates(config.data);
                    }
                }
                return config;
            }
        }

    }])
    .factory('ApiErrorResponseInterceptor', ['$q', '$rootScope', '$log', 'commonUtils', function ($q, $rootScope, $log, commonUtils) {
        return {
            response: function (config) {
                var configResponse = config.config;
                if (configResponse && commonUtils.isUrlForBackend(configResponse.url)) {
                    if (config.data && config.data.error) {
                        if(config.data.code == '_SESSION_EXPIRED_ERROR_CODE') {
                            $rootScope.$emit("SESSION_EXPIRED");
                        }
                        return $q.reject(config.data.error);
                    }
                }
                return config;
            },
            responseError: function(config) {
                if (config.config && config.config.url && config.config.url.indexOf($rootScope.SSEM_BACKEND_REST_DEF) !== -1) {
                    if (config.data) {
                        if (config.data.error) {
                            return $q.reject(config.data.error);
                        }
                    }
                    $log.error(config);
                    return $q.reject('Ocurrió un error en el servidor.');
                }
                return $q.reject(config);
            }
        }
    }])
    /**
     * Interceptor que agrega el header del usuario para los requests/response (a excepcion cuando es login).
     */
    .factory('AuthInterceptor', ['$injector', '$rootScope', 'commonUtils', function ($injector, $rootScope, commonUtils) {
        return {
            request: function(config) {
                if(commonUtils.isUrlForBackend(config.url)) {
                    if(commonUtils.isNotUrlForLogin(config.url)) {
                        if(!angular.isString(config.headers._ssem_user_header)) {
                            var authService = $injector.get('authService');
                            config.headers._ssem_user_header = authService.getHeader();
                        }
                    }
                }
                return config;
            },
            response: function(response) {
                var configResponse = response.config;
                if(configResponse && commonUtils.isUrlForBackend(configResponse.url)) {
                    if(angular.isString(response.headers('_ssem_user_header'))) {
                        var authService = $injector.get('authService');
                        authService.storeUserHeader(response.headers('_ssem_user_header'));
                    }
                }
                return response;
            }
        }
    }])
    .config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('dateFormatInterceptor');
        $httpProvider.interceptors.push('ApiErrorResponseInterceptor');
        $httpProvider.interceptors.push('AuthInterceptor');
    }])

    .filter('equipoByDisciplina', function(){

        return function (equipos, idDisciplina) {
            if(!idDisciplina) return equipos;
            var out = [];
            equipos.forEach(function(s) {
                s.disciplinas.forEach(function(d){
                    if(d.id==idDisciplina)
                        out.push(s);
                })
            });
            return out;
        };

    });