app.directive('ngFileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.ngFileModel);
            var isMultiple = attrs.multiple;
            var modelSetter = model.assign;
            element.bind('change', function () {
                var values = [];
                angular.forEach(element[0].files, function (item) {
                    var reader = new FileReader();
                    reader.readAsDataURL(item);
                    var value = {
                        // File Name
                        name: item.name,
                        //File Size
                        size: item.size,
                        //FileReader para esta img
                        base64: '',
                        // File Input Value
                        _file: item
                    };
                    reader.onloadend = function() {
                        value.base64 = reader.result;
                    };
                    values.push(value);
                });
                scope.$apply(function () {
                    if (isMultiple) {
                        modelSetter(scope, values);
                    } else {
                        modelSetter(scope, values[0]);
                    }
                });
            });
        }
    };
}]);