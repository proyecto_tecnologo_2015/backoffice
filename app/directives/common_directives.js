app.directive("loginForm", function(){
   return {
       restrict: 'AE',
       templateUrl: "app/directives/partials/loginForm.html",
       replace: false
   }
});

app.directive('sidebarOrganizador', function(){
    return {
        restrict: 'AE',
        templateUrl: 'app/directives/partials/sidebar_organizador.html',
        replace:true
    }
});

app.directive('sidebarDelegacion', function(){
   return {
       restrict: 'AE',
       templateUrl: 'app/directives/partials/sidebar_delegacion.html',
       replace: true
   }
});
