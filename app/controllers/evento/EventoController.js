'use strict';
app.controller('eventoController', ['$scope', 'commonService', 'organizadorService', '$rootScope', '$state', 'eventoService', 'authService', 'commonUtils', 'navigationCache',
    function($scope, commonService, organizadorService, $rootScope, $state, eventoService, authService, commonUtils, navigationCache){

        $scope.errors = [];

        if($state.current.data.viewState) {
            $scope.$parent.viewState = $state.current.data.viewState;
        }

        $scope.getDetalleEncuentro = function() {

            var idEvento = navigationCache.getSelectedEvento();
            $scope.evento = { };
            if(!idEvento) {
                $state.go('404');
            }
            else {
                eventoService.getDatosEvento(idEvento).then(
                    function success(data) {
                        $scope.eventoURL = commonUtils.buildUrlEvento(data);
                        data.fechaInicio = commonUtils.parseDateFromArray(data.fechaInicio);
                        data.fechaFin = commonUtils.parseDateFromArray(data.fechaFin);
                        $scope.rellenarOrigenesDatos(data.origenes);
                        $scope.evento = data;
                        $scope.fechaDesde = $scope.evento.fechaInicio;
                        $scope.fechaHasta = $scope.evento.fechaFin;
                    },
                    function error() {
                        $scope.go('404');
                    }
                );
            }
        };

        $scope.initCrearEvento = function() {

            $scope.paises = {};
            $scope.disciplinas = {};
            $scope.errors = [];

            commonService.getPaises()
                .then(
                function(response){
                    $scope.paises = response.data.response;
                },
                function(error) {
                    $scope.errors.push(error);
                }
            );
            //Input de fechas
            $scope.status = {
                fechaDesdeOpened: false,
                fechaHastaOpened: false
            };
            //Abre el DatePicker.
            $scope.openFechaDesde = function($event) {
                $scope.status.fechaDesdeOpened = true;
            };
            $scope.openFechaHasta = function($event) {
                $scope.status.fechaHastaOpened = true;
            };

            $scope.crearEvento = function() {
                if($scope.disciplinas.length == 0) {
                    $scope.errors.push("Debe agregar por lo menos una disciplina.");
                    return false;
                }
                var listaImagenes = [];
                var listaDisciplinas = [];
                $scope.imagenes.forEach(function(imagen){
                    listaImagenes.push(imagen.base64);
                });
                $scope.disciplinas.forEach(function(val) {
                    listaDisciplinas.push(val.nombre);
                });

                var user = authService.getUser();

                var origenes = [];

                if($scope.origenFacebook){
                    var origen = {
                        tipoOrigen : "FACEBOOK",
                        urlApi : $scope.origenFacebook
                    };
                    origenes.push(origen);
                }

                if($scope.origenTwitter){
                    var origen = {
                        tipoOrigen : "TWITTER",
                        urlApi : $scope.origenTwitter
                    };
                    origenes.push(origen);
                }

                if($scope.origenYoutube){
                    var origen = {
                        tipoOrigen : "YOUTUBE",
                        urlApi : $scope.origenYoutube
                    };
                    origenes.push(origen);
                }

                if($scope.origenOtro){
                    var origen = {
                        tipoOrigen : "OTRO",
                        urlApi : $scope.origenOtro
                    };
                    origenes.push(origen);
                }

                var evento = {
                    nombre: $scope.nombre,
                    idOrganizador: user.id,
                    pais: $scope.pais,
                    origenes: origenes,
                    //Se copian las fechas porque no son inmutables y el interceptor cambia el objeto (formateo).
                    fechaInicio: new Date($scope.fechaDesde),
                    fechaFin: new Date($scope.fechaHasta),
                    imagenes: listaImagenes,
                    descripcion: $scope.descripcion,
                    disciplinas: listaDisciplinas
                };

                eventoService.crearEvento(evento)
                    .then(function success(response){
                        $state.go('home.listaEventos');
                    }, function error(response) {
                        $scope.errors = [];
                        $scope.errors.push(response);
                    }
                );

            };
            //Tabla disciplinas
            $scope.disciplinas = [ ];
            var columns =
                [
                    {field: 'nombre', displayName: 'Nombre Disciplina'}
                ];
            $scope.gridOptions = {
                data: $scope.disciplinas,
                columnDefs: columns,
                multiSelect: true,
                noUnselect: false,
                enableRowSelection: true,
                enableRowHeaderSelection: false
            };

            $scope.gridOptions.onRegisterApi = function( gridApi ) {
                $scope.gridApi = gridApi;
            };

            $scope.deleteSelected = function(){
                angular.forEach($scope.gridApi.selection.getSelectedRows(), function (data, index) {
                    $scope.gridOptions.data.splice($scope.gridOptions.data.lastIndexOf(data), 1);
                });
            };

            $scope.agregarDisciplina = function() {
                if($scope.disciplina) {
                    $scope.disciplinas.push({nombre : $scope.disciplina});
                    $scope.disciplina = '';
                    document.getElementById('nombre-disciplina').focus();
                }
            };
        };

        $scope.initListaEventos = function() {
            organizadorService.getEventosOrganizador(authService.getUserId()).then(
                function success(eventos) {
                    $scope.eventos = eventos;
                },
                function error(error) {
                    $scope.errors = [];
                    $scope.errors.push(error);
                }

            );
            $scope.goToEvento = function(evento) {
                navigationCache.setSelectedEvento(evento.idEvento);
                navigationCache.setDatosEvento(evento);
                $state.go('home.evento.detalle');
            }

        };

        $scope.goToEditar = function() {
            $state.go('home.evento.editar');
        };

        $scope.initEditarEvento = function() {

            $scope.errors = [];
            $scope.getDetalleEncuentro();

            //Input de fechas
            $scope.status = {
                fechaDesdeOpened: false,
                fechaHastaOpened: false
            };
            //Abre el DatePicker.
            $scope.openFechaDesde = function($event) {
                $scope.status.fechaDesdeOpened = true;
            };
            $scope.openFechaHasta = function($event) {
                $scope.status.fechaHastaOpened = true;
            };

            $scope.editarEvento = function() {
                var user = authService.getUser();
                var origenes = [];

                $scope.procesarOrigenesDatos(origenes);

                var evento = {
                    idEvento: navigationCache.getSelectedEvento(),
                    nombre: $scope.nombre,
                    origenes:origenes,
                    idOrganizador: user.userId,
                    fechaInicio: new Date($scope.fechaDesde),
                    fechaFin: new Date($scope.fechaHasta),
                    estado: $scope.estado
                };
                eventoService.editarEvento(evento).then(
                    function success(data) {
                        //Actualizamos las fechas del evento...
                        var eventoActualizado = navigationCache.getDatosEvento();
                        eventoActualizado.fechaInicio = data.fechaInicio;
                        eventoActualizado.fechaFin = data.fechaFin;
                        navigationCache.setDatosEvento(eventoActualizado);
                        $state.go('home.evento.detalle');
                    },
                    function error(error) {
                        $scope.errors.push(error);
                    }
                )
            };
        };

        $scope.rellenarOrigenesDatos = function(origenes) {
            if(angular.isArray(origenes)){
                origenes.forEach(function(origen){
                    if(origen.tipoOrigen == 'FACEBOOK') {
                        $scope.origenFacebook = origen.urlApi;
                    }
                    else if(origen.tipoOrigen == 'TWITTER') {
                        $scope.origenTwitter = origen.urlApi;
                    }
                    else if(origen.tipoOrigen == 'YOUTUBE') {
                        $scope.origenYoutube = origen.urlApi;
                    }
                    else if(origen.tipoOrigen == 'OTRO') {
                        $scope.origenOtro = origen.urlApi;
                    }
                });
            }
        };

        $scope.procesarOrigenesDatos = function(origenes) {
            if($scope.origenFacebook){
                var origen = {
                    tipoOrigen : "FACEBOOK",
                    urlApi : $scope.origenFacebook
                };
                if(!commonUtils.esOrigenRepetido($scope.evento.origenes, origen))
                    origenes.push(origen);
            }

            if($scope.origenTwitter){
                var origen = {
                    tipoOrigen : "TWITTER",
                    urlApi : $scope.origenTwitter
                };
                if(!commonUtils.esOrigenRepetido($scope.evento.origenes, origen))
                    origenes.push(origen);
            }

            if($scope.origenYoutube){
                var origen = {
                    tipoOrigen : "YOUTUBE",
                    urlApi : $scope.origenYoutube
                };
                if(!commonUtils.esOrigenRepetido($scope.evento.origenes, origen))
                    origenes.push(origen);
            }

            if($scope.origenOtro){
                var origen = {
                    tipoOrigen : "OTRO",
                    urlApi : $scope.origenOtro
                };
                if(!commonUtils.esOrigenRepetido($scope.evento.origenes, origen))
                    origenes.push(origen);
            }

        };


    }]);