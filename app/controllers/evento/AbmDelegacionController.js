'use strict';

app.controller('ABMDelegacionesController', ['$scope', 'organizadorService', '$state', 'authService','$window', 'commonService', 'navigationCache',
    function($scope, organizadorService, $state, authService,$window, commonService, navigationCache){

        var idEvento = navigationCache.getSelectedEvento();

        $scope.errors = [];

        if(!idEvento) {
            $state.go('404');
        }
        $scope.errors = [];
        $scope.delegaciones = [];
        $scope.delegacion = { };

        function getDelegaciones() {
            organizadorService.getDelegaciones(navigationCache.getSelectedEvento())
                .then(function success(response){
                    $scope.delegaciones = [];
                    response.data.forEach(
                        function(val){
                            $scope.delegaciones.push(val);
                        }
                    );
                    $scope.table.gridOptions.data = $scope.delegaciones;
                },
                function error(response) {
                    $scope.errors.push(response);
                }
            );
        }

        $scope.initDelegaciones = function () {

            $scope.hideForm = true;

            getDelegaciones();

            $scope.openFormDelegacion = function() {
                limpiarDelegacion();
                $scope.hideForm = false;
                $scope.tituloForm = "Alta";
                $scope.operacion = 'ALTA';

                setTimeout(function () {
                    $scope.$apply(function () {
                        $window.scrollTo(0,document.body.scrollHeight);
                    });
                }, 100);


            };

            commonService.getPaises()
                .then(
                function(response){
                    $scope.paises = response.data.response;
                },
                function(error) {
                    $scope.errors.push(error);
                }
            );

            //Tabla
            $scope.table = { };

            var columns = [
                {field: 'nombre', displayName: 'Nombre'},
                {field: 'email', displayName: 'Correo'},
                {
                    name: 'Editar',
                    cellTemplate: '<a href="" ng-click="grid.appScope.editarCelda(row.entity)"><i class="glyphicon glyphicon-edit"></i></a>',
                    cellClass: 'text-center',
                    sortable: false,
                    width: 70
                }
            ];
            $scope.table.gridOptions = {
                data: $scope.delegaciones,
                columnDefs: columns,
                multiSelect: false,
                noUnselect: false,
                modifierKeysToMultiSelect: false,
                enableRowSelection: true,
                enableRowHeaderSelection: false
            };

            $scope.editarCelda = function (entity) {
                if(entity) {
                    $scope.hideForm = false;
                    $scope.delegacion = { nombre: entity.nombre, email: entity.email, pais : entity.pais, id:entity.id}
                    $scope.tituloForm = 'Editar';
                    $scope.operacion = 'EDITAR';
                    $window.scrollTo(0,0);
                }
            };

            $scope.submitDelegacion = function() {
                if($scope.operacion === 'EDITAR') {
                    editarDelegacion();
                }
                else if($scope.operacion === 'ALTA') {
                    crearDelegacion();
                }
            };

            $scope.ocultarForm = function() {
                $scope.hideForm = true;
            };

            $scope.table.gridOptions.onRegisterApi = function( gridApi ) {
                $scope.table.gridApi = gridApi;
            };

            function limpiarDelegacion() {
                $scope.delegacion.nombre = '';
                $scope.delegacion.email = '';
                $scope.delegacion.pais = '';
                delete $scope.delegacion.id;
            }

            function editarDelegacion() {
                organizadorService.editarOperador($scope.delegacion).then(
                    function success(){
                        getDelegaciones();
                        $scope.hideForm = true;
                    },
                    function error(error) {
                        $scope.errors.push(error);
                    }
                );
            }

            function crearDelegacion() {
                $scope.delegacion.idEvento = navigationCache.getSelectedEvento();
                if(!$scope.delegacion.pais)  {
                    $scope.errors = ['Debe seleccionar un pais'];
                    return;
                }
                $scope.errors = [];
                organizadorService.crearDelegacion($scope.delegacion).then(
                    function success() {
                        getDelegaciones();
                        $scope.hideForm = true;
                    },
                    function error(error) {
                        $scope.errors.push(error);
                    }
                )
            }
            // $interval whilst we wait for the grid to digest the data we just gave it
            //$interval( function() {            $scope.gridApi.selection.selectRow($scope.table.gridOptions.data[0]);}, 0, 1);


        }

    }]);
