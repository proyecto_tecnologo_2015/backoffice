'use strict';

app.controller('NovedadesController', ['$scope', '$state', 'novedadService', 'navigationCache', '$injector', '$rootScope', 'commonUtils',

    function ($scope, $state, novedadService, navigationCache, $injector, $rootScope, commonUtils) {

        var FORM_ALTA_NOVEDAD = 'home.evento.altaNovedad';
        //var FORM_EDITAR_NOVEDAD = 'home.evento.editarNovedad'
        $scope.submitFormDelegaciones = function () {
            createNovedad();
        };

        function createNovedad() {
            $scope.novedad.idEvento = navigationCache.getSelectedEvento();
            if (!$scope.novedad.idEvento) {
                $state.go('home.listaEventos');
            }
            $scope.novedad.fecha = new Date();
            novedadService.altaNovedad($scope.novedad).then(
                function success() {
                    $state.go('home.evento.listaNovedades');
                },
                function error(error) {
                    $scope.errors.push(error);
                }
            );
        }

        $scope.initFlujoNovedades = function () {
            if ($state.current.name === FORM_ALTA_NOVEDAD) {
                $scope.novedad = {};
                $scope.fechaOpened = false;
                $scope.tituloForm = 'Alta Novedad';
                $scope.previsualizar = false;
            }
            else {

            }
        };

        $scope.initDetalle = function() {
            var novedad = navigationCache.get('abmNovedad.detalle.novedad');
            novedadService.getDetalleNovedad(novedad).then(
                function(novedad) {
                    novedad.fecha = commonUtils.parseDateFromArray(novedad.fecha);
                    $scope.novedad = novedad;
                },
                function(error) {
                    $scope.errors = [error];
                }
            )

        };

        $scope.initListaNovedades = function () {
            $scope.errors = [];

            $scope.detalle = function(novedad){
                navigationCache.put('abmNovedad.detalle.novedad', novedad.id);
                $state.go('home.evento.detalleNovedad')
            };

            var idEvento = navigationCache.getSelectedEvento();
            if (!idEvento) {
                $state.go('home.listaEventos');
            }
            novedadService.getNovedadesEvento(idEvento).then(
                function success(data) {
                    $scope.novedades = [];
                    $scope.novedades = data;
                    $scope.novedades.forEach(function (val) {
                        if (val.fecha)
                            val.fecha = commonUtils.parseDateFromArray(val.fecha);
                    });
                    var tableUtils = $injector.get('tableUtils');
                    var columns = [
                        {field: 'titulo', displayName: 'Título'},
                        {
                            field: 'fecha', displayName: 'Fecha',
                            cellFilter: 'date: "dd/MM/yyyy"',
                            width: 150
                        },
                        tableUtils.columnHelper.newDetailColumn('detalle')
                    ];
                    $scope.table = {};
                    tableUtils.newDefaultTable($scope, columns, $scope.novedades);
                }
            );


        };


    }]);
