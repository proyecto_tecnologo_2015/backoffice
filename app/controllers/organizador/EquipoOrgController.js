/**
 * Created by dalepo on 07/05/16.
 */
app.controller('EquipoOrgController', ['$scope', 'equipoService', '$state', 'tableUtils', 'commonService', 'navigationCache', '$filter',
function($scope, equipoService, $state, tableUtils, commonService, navigationCache, $filter) {

    $scope.initLista = function() {
        $scope.hideButton = true;
        $scope.equipos = [];

        $scope.detalle = function(equipo) {
            navigationCache.put('equipo.detalle.equipo', equipo.id);
            $state.go('home.detalleEquipo');
        };

        var evento = navigationCache.getSelectedEvento();

        equipoService.getListaEquipos(evento)
            .then(
                function success(data) {
                    $scope.errors = [];
                    $scope.tieneEquipos = data.length > 0;
                    if(angular.isArray(data)) {
                        var sorter = $filter('orderBy');
                        data = sorter(data, 'delegacion.nombre');
                        data.forEach(function(value){
                            $scope.equipos.push(value);
                        });
                    }
                },
                function error(errors) {
                    $scope.errors = [];
                    $scope.errors.push(errors);
                });

        $scope.table = { };

        var columns = [
            {field: 'nombre', displayName: 'Nombre'},
            {field: 'delegacion.nombre', displayName: 'Delegación'},
            tableUtils.columnHelper.newDetailColumn('detalle')
        ];

        tableUtils.newDefaultTable($scope, columns, $scope.equipos);

    };

    $scope.initDetalle = function () {
        $scope.backUrl = 'home.equipos';
        var idEquipo = navigationCache.get('equipo.detalle.equipo');
        equipoService.getDetalleEquipo(idEquipo).then(
            function(equipo){
                $scope.equipo = equipo;
            },
            function(error) {
                $scope.errors = [error];
            }
        )
    };

}]);