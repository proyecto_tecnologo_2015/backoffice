/**
 * Created by dalepo on 07/05/16.
 */
app.controller('DeportistaOrgController', ['$scope', 'navigationCache', 'equipoService', 'tableUtils', '$state', 'commonUtils','uiGridConstants', function($scope, navigationCache, equipoService, tableUtils, $state, commonUtils, uiGridConstants) {

    $scope.initDetalle = function() {
        $scope.backUrl = 'home.listaDeportistas';
        var idDeportista = navigationCache.get('deportista.detalle.deportista');
        if (idDeportista != null) {
            equipoService.getDetalleDeportista(idDeportista).then(
                function(deportista) {
                    deportista.fechanac = commonUtils.parseDateFromArray(deportista.fechanac);
                    $scope.deportista = deportista;
                },
                function(error) {
                    $scope.errors = [error];
                }
            )
        }
    };

    $scope.initLista = function() {

        $scope.deportistas = [];
        $scope.equipos = [];
        $scope.errors = [];
        $scope.hideBottonAgregar = true;

        var evento = navigationCache.getSelectedEvento();

        $scope.detalle = function(deportista) {
            navigationCache.put('deportista.detalle.deportista', deportista.id);
            $state.go('home.detalleDeportista');
        };

        equipoService.getDeportistasEventoOrganizador(evento).then(
            function success(data) {
                if(data){
                    $scope.tieneDeportistas = data.length > 0;
                    data.forEach(function(val) {
                        if(angular.isArray(val.equipo)) {
                            val.equipo = val.equipo[0];
                        }
                        $scope.deportistas.push(val);
                    });
                }
            },
            function error(errors) {
                $scope.errors.push(errors);
            }
        );

        $scope.table = { };

        var columns = [
            {field: 'nombre', displayName: 'Nombre', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.nombre}} {{ row.entity.apellido}}</div>'},
            {field: 'delegacion.nombre', displayName: 'Delegación'},
            {field: 'equipo.nombre', displayName: 'Equipo'},
            tableUtils.columnHelper.newDetailColumn('detalle')
        ];
        tableUtils.newDefaultTable($scope, columns, $scope.deportistas);
        //$scope.gridApi.grid.sortColumn(columns[0], uiGridConstants.ASC, add)
    };

}]);