'use strict';


app.controller('RegisterOrganizadorController', [ '$scope', 'commonService', 'organizadorService', 'modalService', '$state',
    function($scope, commonService, organizadorService, modalService, $state) {

        $scope.fechaNacOpened = false;
        $scope.request = {};
        $scope.errors = [];
        commonService.getPaises()
            .then(
            function(response){
                $scope.paises = response.data.response;
            },
            function(error) {
                $scope.errors.push(error);
            }
        );

        $scope.openFechaNac = function() {
            $scope.fechaNacOpened = true;
        };

        $scope.registrarOrganizador = function(){
            $scope.request.request.imagen = $scope.imagen.base64;
            $scope.request.request.tipoPerfil = "ORGANIZADOR";
            organizadorService.registrarOrganizador($scope.request)
                .then(function(){
                    modalService.successModal().result
                        .then(function(){
                            $scope.close;
                            $state.go('entry');
                        });
                },
                function error(errors) {
                    $scope.errors.push(errors);
                }
            );
        };

    }]);