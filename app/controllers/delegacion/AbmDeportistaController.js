app.controller('AbmDeportistaController', ['$scope', 'navigationCache', 'equipoService', 'tableUtils', '$state', 'commonUtils', function($scope, navigationCache, equipoService, tableUtils, $state, commonUtils) {

    $scope.initDetalle = function() {
        $scope.backUrl = 'homeDelegacion.listaDeportistas';
        var idDeportista = navigationCache.get('abmDeportista.detalle.deportista');
        if (idDeportista != null) {
            equipoService.getDetalleDeportista(idDeportista).then(
                function(deportista) {
                    deportista.fechanac = commonUtils.parseDateFromArray(deportista.fechanac);
                    $scope.deportista = deportista;
                },
                function(error) {
                    $scope.errors = [error];
                }
            )
        }
    };

    $scope.initLista = function() {

        $scope.deportistas = [];
        $scope.equipos = [];
        $scope.errors = [];
        $scope.hideBottonAgregar = false;

        var evento = navigationCache.getEventoDelegacion();

        $scope.detalle = function(deportista) {
            navigationCache.put('abmDeportista.detalle.deportista', deportista.id);
            $state.go('homeDelegacion.detalleDeportista');
        };

        equipoService.getDeportistasEvento(evento.idEvento).then(
            function success(data) {
                if(data){
                    $scope.tieneDeportistas = data.length > 0;
                    data.forEach(function(val) {
                       $scope.deportistas.push(val);
                    });
                }
            },
            function error(errors) {
                $scope.errors.push(errors);
            }
        );

        $scope.table = { };

        var columns = [
            {field: 'nombre', displayName: 'Nombre', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.nombre}} {{ row.entity.apellido}}</div>'},
            {field: 'equipo.nombre', displayName: 'Equipo'},
            tableUtils.columnHelper.newDetailColumn('detalle')
        ];

        tableUtils.newDefaultTable($scope, columns, $scope.deportistas)

    };

    $scope.initAlta = function () {

        var evento = navigationCache.getEventoDelegacion();
        $scope.equipos = [];
        $scope.deportista = { };
        equipoService.getEquiposDelegacion().then(
            function success(data) {
                if(angular.isArray(data)) {
                    data.forEach(function(val){
                        $scope.equipos.push(val);
                    });
                }
            },
            function error(errors) {
                $scope.errors.push(errors);
            }
        );

        $scope.submitAlta = function() {
            if($scope.imagen)
                $scope.deportista.imagen = $scope.imagen.base64;
            if(!$scope.deportista.equipos) {
                $scope.errors = ['Debe seleccionar por lo menos un equipo'];
                return;
            }
            equipoService.altaDeportista($scope.deportista).then(
                function success(){
                    $state.go('homeDelegacion.listaDeportistas');
                },
                function error(errors) {
                    $scope.errors = [];
                    $scope.errors.push(errors);
                }
            );
        };

    };




}]);