/**
 * Created by dalepo on 22/04/16.
 */
app.controller('PerfilController', ['$scope', 'authService', 'organizadorService', 'modalService', function($scope, authService, organizadorService, modalService){

    var user = authService.getUser();

    $scope.confirmPassword = '';
    $scope.passwordMatches = false;
    $scope.correo = user.email;
    $scope.styleConfirmPassword = '';


    $scope.operador =  {
        nombre: user.nombre,
        id: user.id
    };

    $scope.focusConfirmPassword = function () {
        if($scope.confirmPassword == false)
            $scope.styleConfirmPassword = {'border-color' : 'red'}
    };

    $scope.comparePassword = function () {
        if($scope.confirmPassword === $scope.operador.password) {
            $scope.passwordMatches = true;
            $scope.styleConfirmPassword = {}
        }
        else {
            $scope.passwordMatches = false;
            $scope.styleConfirmPassword = {'border-color' : 'red'};
        }
    };

    $scope.editar = function() {
        organizadorService.editarOperador($scope.operador).then(
            function(data) {
                modalService.successModal();
            },
            function(error) {
                $scope.errors = [error];
            }
        )
    };

}]);
