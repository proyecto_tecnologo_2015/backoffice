'use strict';

app.controller('AbmEquipoController', ['$scope', 'equipoService', '$state', 'tableUtils', 'commonService', 'navigationCache', function($scope, equipoService, $state, tableUtils, commonService, navigationCache) {

    $scope.errors = [];

    $scope.initAlta = function() {

        $scope.equipo =  {
            disciplinas: []
        };

        $scope.disciplinasAlta = [];

        var evento = navigationCache.getEventoDelegacion();

        $scope.disciplinas = evento.disciplinas;

        $scope.submitAlta = function() {
            if(!$scope.disciplinasAlta || $scope.disciplinasAlta == 0){
                $scope.errors.push('Debe seleccionar al menos una disciplina.');
                return;
            }
            $scope.disciplinasAlta.forEach(function(value){
                $scope.equipo.disciplinas.push({
                    id : value.id
                });
            });
            equipoService.altaEquipo($scope.equipo).then(
                function success() {
                    $state.go('homeDelegacion.listaEquipos');
                },
                function error(errors) {
                    $scope.errors = [];
                    $scope.errors.push(errors);
                }
            );
        };

    };

    $scope.initDetalle = function () {
        $scope.backUrl = 'homeDelegacion.listaEquipos';
        var idEquipo = navigationCache.get('abmEquipo.detalle.equipo');
        equipoService.getDetalleEquipo(idEquipo).then(
            function(equipo){
                $scope.equipo = equipo;
            },
            function(error) {
                $scope.errors = [error];
            }
        )
    };

    $scope.initLista = function() {

        $scope.equipos = [];
        $scope.hideButton = false;

        $scope.detalle = function(equipo) {
            navigationCache.put('abmEquipo.detalle.equipo', equipo.id);
            $state.go('homeDelegacion.detalleEquipo');
        };

        equipoService.getEquiposDelegacion()
            .then(
            function success(data) {
                $scope.errors = [];
                $scope.tieneEquipos = data.length > 0;
                data.forEach(function(value){
                    $scope.equipos.push(value);
                });
            },
            function error(errors) {
                $scope.errors = [];
                $scope.errors.push(errors);
            });

        $scope.table = { };

        var columns = [
            {field: 'nombre', displayName: 'Nombre'},
            tableUtils.columnHelper.newDetailColumn('detalle')
        ];

        tableUtils.newDefaultTable($scope, columns, $scope.equipos);

    };

}]);