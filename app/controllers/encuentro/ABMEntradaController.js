/**
 * Created by dalepo on 11/05/16.
 */
app.controller('AbmEntradaController', ['$scope', '$timeout', 'entradaService', '$state', 'navigationCache', '$cookies', 'tableUtils',
function($scope, $timeout, entradaService, $state, navigationCache, $cookies, tableUtils){

    $scope.initLista = function() {

        $scope.listaEntradas = [];
        $scope.terminoEncuentro = $cookies.get('encuentro.detalleEncuentro.terminoEncuentro') === 'true';
        $scope.enablePopOver = $scope.terminoEncuentro;

        $scope.popOver = {
            content: 'El encuentro ya ha finalizado.'
        };

        var idEncuentro = $cookies.get('encuentro.detalleEncuentro.idEncuentro');

        entradaService.getEntradasEncuentro(idEncuentro).then(
            function (data) {
                $scope.tieneEntradas = data.length > 0;
                if(angular.isArray(data)) {
                    data.forEach(function(entrada){
                       $scope.listaEntradas.push(entrada);
                    });
                }
            },
            function (error) {
                $scope.errors = [error];
            }
        );

        $scope.table = { };

        var columns = [
            {field: 'nombre', displayName: 'Nombre'},
            {field: 'precio', displayName: 'Precio', cellTemplate: '<div class="ui-grid-cell-contents ng-binding ng-scope">U$S&nbsp;{{row.entity.precio}}</div>'},
            {field: 'stock', displayName: 'Stock'},
            {field: 'stockinicial', displayName: 'Stock Inicial'},
            {field: 'habilitada', displayName: 'Habilitada', cellTemplate: '<div class="ui-grid-cell-contents ng-binding ng-scope">{{row.entity.habilitada == true ? "Si" : "No" }}</div>'}
        ];
        tableUtils.newDefaultTable($scope, columns, $scope.listaEntradas);

        $scope.goAltaEntrada = function() {
            if($scope.terminoEncuentro === false)
                $state.go('home.encuentro.altaEntrada');
        };

    };

    $scope.initAlta = function () {
        $scope.entrada = {};
        $scope.entradas = [];
        $scope.esconderEntradas = true;
        $scope.idEncuentro = $cookies.get('encuentro.detalleEncuentro.idEncuentro');
        $scope.removeEntrada = function(index) {
            if($scope.entradas.length == 1) {
                $scope.esconderEntradas = true;
                $timeout(function(){
                    $scope.entradas.splice(index, 1);
                }, 1000);
            }
            else {
                $scope.entradas.splice(index, 1);
            }
        };

        $scope.addEntrada = function() {
            $scope.esconderEntradas = false;
            $scope.entrada.idEncuentro = $scope.idEncuentro;
            $scope.entradas.push($scope.entrada);
            $scope.entrada = {};
        };

        $scope.alta = function () {
            if($scope.entradas.length < 1) {
                $scope.errors = ['Debe agregar por lo menos una entrada.'];
                return;
            }
            entradaService.altaEntradas($scope.entradas).then(
                function(){
                    $state.go('home.encuentro.listaEntradas');
                },
                function (error) {
                    $scope.errors = [error];
                }
            )
        };

    };

}]);