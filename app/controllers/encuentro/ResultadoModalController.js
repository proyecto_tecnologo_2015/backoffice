app.controller('ResultadoModalController', ['$scope', '$uibModalInstance', 'tableUtils', '$filter', function ($scope, $uibModalInstance, tableUtils, $filter) {

    $scope.resultados = $scope.encuentro ? $scope.encuentro.resultados : undefined;

    if(!$scope.resultados || $scope.resultados.length == 0) {
        $scope.tieneResultados = false;
    }
    else
        $scope.tieneResultados = true;
    if($scope.tieneResultados) {
        var resultado = $scope.resultados[0];
        if(resultado.puntaje)
            $scope.tienePuntajes = true;
        else
            $scope.tienePuntajes = false;

        $scope.table = { };
        var sorter = $filter('orderBy');
        if($scope.tienePuntajes) {
            $scope.resultados = sorter($scope.resultados, 'puntaje', true);
            var columns = [
                {field: 'puntaje', displayName: 'Puntaje', width: '35%'},
                {field: 'equipo.nombre', displayName: 'Equipo'}
            ];
        }
        else {
            $scope.resultados = sorter($scope.resultados, 'posicion', false);
            var columns = [
                {field: 'posicion', displayName: 'Posición', width: '35%'},
                {field: 'equipo.nombre', displayName: 'Equipo'}
            ];
        }
        tableUtils.newDefaultTable($scope, columns, $scope.resultados);
    }

    $scope.close = function() {
        $uibModalInstance.close();
    }
}]);