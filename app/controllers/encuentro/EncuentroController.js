app.controller('EncuentroController',

['$scope', '$state', '$stateParams', 'encuentroService', '$rootScope', 'equipoService', 'navigationCache', 'eventoService', 'encuentroValidator', '$filter', 'equipoByDisciplinaFilter', 'tableUtils', 'commonUtils', '$uibModal', '$cookies', 'eventoUtils',
function ($scope, $state, $stateParams, encuentroService, $rootScope, equipoService, navigationCache, eventoService, encuentroValidator, $filter, equipoByDisciplinaFilter, tableUtils, commonUtils, $uibModal, $cookies, eventoUtils) {

    $scope.currentStep = '';

    $scope.goBack = function () {
        navigationCache.memoryCache.put('encuentro', $scope.encuentro);
        switch ($scope.currentStep) {
            case 1:
                $state.go('home.encuentro.listaEncuentros');
                break;
            case 2:
                $state.go('home.encuentro.altaEncuentro_1');
                break;
            case 3:
                $state.go('home.encuentro.altaEncuentro_2');
                break;
            default:
                $state.go('home.encuentro.listaEncuentros');
                break;
        }
    };

    $scope.goNext = function () {
        switch ($scope.currentStep) {
            case 1:
                $scope.errors = encuentroValidator.validateStepOne($scope.encuentro);
                navigationCache.memoryCache.put('encuentro', $scope.encuentro);
                if($scope.errors.length == 0)
                    $state.go('home.encuentro.altaEncuentro_2');
                break;
            case 2:
                $scope.errors = encuentroValidator.validateStepTwo($scope.encuentro);
                if($scope.errors.length > 0)
                    return;
                navigationCache.memoryCache.put('encuentro', $scope.encuentro);
                $state.go('home.encuentro.altaEncuentro_3');
                break;
            case 3:
                if(!$scope.marker || !$scope.marker.coords) {
                    $scope.errors = ['Debes marcar la localización del evento.'];
                }
                else {
                    $scope.encuentro.ubicacion = {lat: $scope.marker.coords.latitude, lng: $scope.marker.coords.longitude };
                    encuentroService.altaEncuentro($scope.encuentro).then(
                        function success() {
                            $state.go('home.encuentro.listaEncuentros');
                        },
                        function error(error) {
                            $scope.errors = [error];
                        }
                    );
                }

                break;
            default:
                break;
        }
    };


    $scope.initLista = function () {
        $scope.cargoEncuentros = null;
        $scope.encuentros = [];
        //Se limpia el cache
        navigationCache.memoryCache.remove('encuentro');
        var idEvento = navigationCache.getSelectedEvento();
        var datosEvento = navigationCache.getDatosEvento();
        $scope.isEventoFinalizado = eventoUtils.isEventoFinalizado(datosEvento);
        $scope.popOver = {
            content: 'El evento ya ha finalizado.'
        };
        //Funciones botones tabla.
        $scope.detail = function(encuentro) {
            $cookies.put('encuentro.detalleEncuentro.idEncuentro', encuentro.id);
            $state.go('home.encuentro.detalleEncuentro');
        };

        $scope.goAltaEncuentro = function () {

            if(!$scope.isEventoFinalizado)
                $state.go('home.encuentro.altaEncuentro_1')
        };

        encuentroService.getEncuentrosEvento(idEvento).then(
            function success(data) {
                $scope.tieneEncuentros = data.length > 0;
                if(angular.isArray(data)){
                    data.forEach(function(val){
                        val.fecha = commonUtils.parseDateFromArray(val.fecha);
                        val.fechafin = commonUtils.parseDateFromArray(val.fechafin);
                        $scope.encuentros.push(val);
                    })
                }
            },
            function error(error) {
                $scope.errors = [error];
            }
        );

        $scope.table = { };

        var columns = [
            {field: 'nombre', displayName: 'Nombre'},
            tableUtils.columnHelper.newDateColumn('fecha', 'Fecha Inicio', $rootScope.dateFormat),
            tableUtils.columnHelper.newDateColumn('fechafin', 'Fecha Fin', $rootScope.dateFormat),
            tableUtils.columnHelper.newDetailColumn('detail')
        ];

        tableUtils.newDefaultTable($scope, columns, $scope.encuentros);
    };

    $scope.initDetalleEncuentro = function () {
        $scope.encuentro = null;

        $scope.map = {
            center: {
                latitude: 56.162939,
                longitude: 10.203921
            },
            zoom: 12
        };

        var idEncuentro = $cookies.get('encuentro.detalleEncuentro.idEncuentro');
        var idEvento = navigationCache.getSelectedEvento();

        eventoService.getDisciplinasEvento(idEvento).then(
            function success(data) {
                $scope.disciplinas = data;
            },
            function error(error) {
                $scope.errors.push(error);
            }
        );

        encuentroService.getDetalleEncuentro(idEncuentro).then(
            function success(data) {
                data.fecha = commonUtils.parseDateFromArray(data.fecha);
                data.fechafin = commonUtils.parseDateFromArray(data.fechafin);
                $scope.encuentro = data;
                var location = $scope.encuentro.ubicacion;
                if(location){
                    $scope.marker = {
                        id: 1,
                        coords: {
                            latitude: location.lat,
                            longitude: location.lng
                        }

                    };
                    $scope.map.center = angular.copy($scope.marker.coords);
                }
            },
            function error(error) {
                $scope.errors = [error];
            }
        );

        $scope.goListaEntradas = function () {
            var terminoEncuentro = encuentroValidator.terminoEncuentro($scope.encuentro);
            $cookies.put('encuentro.detalleEncuentro.terminoEncuentro', terminoEncuentro);
            $state.go('home.encuentro.listaEntradas');
        };

        $scope.openModalResultado = function () {
            $uibModal.open({
                animation: true,
                backdrop: 'static',
                scope: $scope,
                templateUrl: 'views/modals/modal-resultado.html',
                controller: 'ResultadoModalController',
                windowClass: 'center-modal-resultado',
                size: 'md'
            });
        };

    };

    $scope.initStepOne = function () {
        $scope.encuentro = navigationCache.memoryCache.get('encuentro');
        if(!$scope.encuentro)
            $scope.encuentro = {};
        $scope.errors = [];
        $scope.currentStep = 1;
        var idEvento = navigationCache.getSelectedEvento();
        eventoService.getDisciplinasEvento(idEvento).then(
            function success(data) {
                $scope.disciplinas = data;
            },
            function error(error) {
                $scope.errors.push(error);
            }
        );

    };

    $scope.initStepTwo = function () {
        $scope.encuentro = navigationCache.memoryCache.get('encuentro');
        $scope.encuentro.equipos = [];
        $scope.errors = [];
        $scope.currentStep = 2;
        var idEvento = navigationCache.getSelectedEvento();
        equipoService.getEquiposEvento(idEvento).then(
            function success(data) {
                if(angular.isArray(data)) {
                    $scope.equipos = equipoByDisciplinaFilter(data, $scope.encuentro.idDisciplina);
                    if($scope.equipos.length > 0)
                        $scope.tieneEquipos = true;
                    else {
                        $scope.tieneEquipos = false;
                    }
                }
                else {
                    $scope.tieneEquipos = false;
                }
            },
            function error(error) {
                $scope.errors.push(error);
            }
        );

        $scope.dropSuccessHandler = function($event,index,array){
            array.splice(index,1);
        };

        $scope.onDrop = function($event,$data,array){
            array.push($data);
        };

    };

    $scope.initStepThree = function () {

        $scope.encuentro = navigationCache.memoryCache.get('encuentro');
        $scope.errors = [];
        $scope.currentStep = 3;
        $scope.marker = null;
        $scope.iconoCopa = 'cup.png';

        var searchBoxEvent =  {
            places_changed: function(searchBox) {
                var place = searchBox.getPlaces();
                if (!place || place == 'undefined' || place.length == 0) {
                    return;
                }
                $scope.map.center.latitude = place[0].geometry.location.lat();
                $scope.map.center.longitude = place[0].geometry.location.lng();
                $scope.map.zoom = 15;
            }
        };

        var mapEvents = {
            click: function(map, eventName, originalEventArgs) {
                var e = originalEventArgs[0];
                var lat = e.latLng.lat();
                var lon = e.latLng.lng();
                $scope.marker = {
                    id: 1,
                    coords: {
                        latitude: lat,
                        longitude: lon
                    },
                    icon: $rootScope.ICONS_PATH + $scope.iconoCopa
                };
                $scope.$apply();
            }
        };

        var datosEvento = navigationCache.getDatosEvento();
        var center = {};
        if(datosEvento && datosEvento.pais) {
            center.latitude = datosEvento.pais.ubicacion.lat;
            center.longitude = datosEvento.pais.ubicacion.lng;
        }
        else {
            center.latitude = -34.921337;
            center.longitude = -56.161663;
        }
        $scope.map = {
            center: center,
            zoom: 6,
            events: mapEvents,
            searchBox : {
                template: 'searchbox.tpl.html',
                events: searchBoxEvent,
                position: 'TOP_RIGHT'
            }
        };
    };

}]);