/**
 * Created by dalepo on 15/05/16.
 */
app.controller('ReporteVentasController', ['$scope', 'encuentroService', 'navigationCache', 'reporteService', function ($scope, encuentroService, navigationCache, reporteService) {

    $scope.encuentros = [];
    $scope.estaCargandoGrafica = false;
    $scope.filter = {};
    $scope.mostrarReporte = false;
    $scope.chart = {};
    var idEvento = navigationCache.getSelectedEvento();

    encuentroService.getEncuentrosEvento(idEvento).then(
        function success(data) {
            if(angular.isArray(data)) {
                $scope.encuentros = data;
            }
        },
        function error(error) {
            $scope.errors = [error];
        }
    );

    $scope.cargarGrafica = function() {
        $scope.errors = [];
        validateFilter($scope.filter, $scope.errors);
        if($scope.errors.length > 0) {
            return;
        }
        reporteService.getReporteEntradasEncuentro(angular.copy($scope.filter)).then(
            function success(data) {
                if(angular.isArray(data)) {
                    cargarJChart(data);
                    $scope.mostrarReporte = true;
                }
                else {
                    $scope.mostrarReporte = false;
                    $scope.errors.push('No hay datos para el período seleccionado.')
                }
            },
            function (error) {
                $scope.erros = [error];
            }
        )
    };

    function cargarJChart(data) {
        var chart = {};
        chart.labels = [];
        chart.data = [];
        chart.options = {

        };
        data.forEach(function (val) {
            chart.labels.push(val.nombre)
            chart.data.push(val.cantidad);
        });
        $scope.chart = chart;
    }

    function validateFilter (filter, errors) {
        if(!filter.idEncuentro) {
            errors.push('Debe seleccionar un encuentro.');
            return false;
        }
        if(!filter.finicio instanceof Date) {
            errors.push('Fecha desde invalida');
            return false;
        }
        if(!filter.ffinal instanceof Date) {
            errors.push('Fecha hasta invalida');
        }
        else if(filter.finicio > filter.ffinal) {
            errors.push('Fecha desde no puede ser mayor a Fecha hasta.')
        }
    }

}]);