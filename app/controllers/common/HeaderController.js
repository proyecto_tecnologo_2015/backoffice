'use strict';

/**
 * Controlador para el form de registro para los organizadores.
 * Abre un modal que tiene un controller independiente.
 * Se encarga de bindear la funcion openModal.
 */
app.controller('HeaderController', ['$scope', '$uibModal', 'authService', 'commonService', '$rootScope', '$state', 'navigationCache',
    function($scope, $uibModal, authService, commonService, $rootScope, $state, navigationCache){

        $scope.isLoggedIn = authService.isAuthenticated();

        /**
         * Cuando hay un evento de logeo se busca el detalle de la cuenta.
         */
        $rootScope.$on('#logeoEvent', function(){

            $scope.isLoggedIn = authService.isAuthenticated();

            if($scope.isLoggedIn) {
                commonService.getDatosOperador(authService.getUserId()).then(
                    function success(data) {
                        $scope.user = data;
                    }
                );
            }

        });

        $scope.openModalLogin = function () {

            $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: 'modal-login.html',
                controller: 'LoginController',
                size: 'sm'
            });

        };

        $scope.logout = function() {
            navigationCache.cleanCache();
            authService.logout();
            $state.go('index' );
        }

    }]);