'use strict';
app.controller('mainController', ['$scope', '$state',
    function($scope, $state){

        $scope.isCollapsed = true;

        $scope.viewState = $state.current.data.viewState;

        $scope.nuevoEvento = function() {
            $state.go('home.altaEvento');
        };
        $scope.listaEventos = function() {
            $state.go('home.listaEventos');
        };

        $scope.listaDelegaciones = function() {
            $state.go('home.evento.delegaciones');
        };

        $scope.volverHome = function() {
            $scope.viewState = 'HOME';
            $state.go('home.listaEventos');
        };
        
        $scope.goSelectedEvento = function() {
            $state.go('home.evento.detalle');
        };

    }]);