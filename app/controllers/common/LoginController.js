'use strict';
app.controller('LoginController', ['$scope', '$uibModalInstance', 'loginService', 'authService', 'navigationCache',
    function($scope, $uibModalInstance, loginService, authService, navigationCache) {
        $scope.errors = [];
        $scope.user = {};

        $scope.close = function () {
            $uibModalInstance.close();
        };

        $scope.doLogin = function() {
            loginService.doLogin($scope.user).then(
                function success() {
                    navigationCache.cleanCache();
                    authService.redirectUserStart();
                    $scope.close();
                },
                function error(error) {
                    $scope.errors = [];
                    $scope.errors.push(error);
                }
            );
        }

    }]);