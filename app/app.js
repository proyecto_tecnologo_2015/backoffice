'use strict';

var app = angular.module('ssemBackoffice',
    [
        'ui.bootstrap',
        'ui.router',
        'ngAnimate',
        'ngMessages',
        'ui.grid',
        'ui.grid.selection',
        'ui.grid.edit',
        'ngCookies',
        'uiGmapgoogle-maps',
        'ang-drag-drop',
        'chart.js',
        //Libs para inputs
        'isteven-multi-select',
        'ui.bootstrap.datetimepicker',
        'textAngular',
        //QR
        'ja.qr',
        //Custom nuestras
        'AppInterceptors'
    ]
);

app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('registrar', {
            url:'/registrar',
            templateUrl: 'views/registrar.html',
            controller: 'RegisterOrganizadorController'
        })
        .state('home', {
            url: '/home',
            templateUrl: 'views/home/home.html',
            controller: 'mainController',
            data: {
                roles: 'ORGANIZADOR',
                viewState: "HOME"
            }
        })
        .state('home.altaEvento', {
            url: '/altaEvento',
            templateUrl: 'views/evento/crearEvento.html',
            controller: 'eventoController'
        })
        .state('home.listaEventos', {
            url: '/listaEventos',
            templateUrl: 'views/evento/listaEventos.html',
            controller: 'eventoController'
        })
        .state('home.evento', {
            url: '/evento',
            abstract: true,
            template: '<section id="transition-evento" ui-view ng-animate="\'view\'"/>',
            controller: 'eventoController',
            data: {
                viewState: "EVENTO"
            }
        })
        .state('home.evento.detalle', {
            url: '/detalle',
            templateUrl: "views/evento/detalleEvento.html",
            controller: 'eventoController',
            data: {
                viewState: "EVENTO"
            }
        })
        .state('home.evento.editar', {
            url:'/editar',
            templateUrl: 'views/evento/editarEvento.html',
            controller: 'eventoController'
        })
        .state('home.evento.delegaciones', {
            url: '/delegaciones',
            templateUrl: 'views/delegaciones/abmDelegacion.html',
            controller: 'ABMDelegacionesController',
            data: {
                viewState: "DELEGACIONES"
            }
        })
        .state('home.evento.listaNovedades', {
            url: '/novedades',
            templateUrl: 'views/abmNovedades/listaNovedades.html',
            controller: 'NovedadesController',
            data: {
                viewState: 'NOVEDADES'
            }
        })
        .state('home.evento.altaNovedad', {
            url: '/altanovedad',
            templateUrl: 'views/abmNovedades/novedad.html',
            controller: 'NovedadesController',
            data: {
                viewState: 'NOVEDADES'
            }
        })
        .state('home.evento.detalleNovedad', {
            url: '/detalleNovedad',
            templateUrl: 'views/abmNovedades/detalle_novedad.html',
            controller: 'NovedadesController',
            data: {
                viewState: 'NOVEDADES'
            }
        })
        .state('home.encuentro', {
            url: '/encuentros',
            templateUrl: 'views/encuentro/encuentro.html',
            controller: 'EncuentroController',
            data: {
                viewState: 'ENCUENTROS'
            }
        })
        .state('home.encuentro.listaEncuentros', {
            url: '/lista',
            templateUrl: 'views/encuentro/lista_encuentros.html',
            controller: 'EncuentroController'
        })
        .state('home.encuentro.altaEncuentro_1', {
            url: '/altaEncuentro',
            templateUrl: 'views/encuentro/alta_step_one.html',
            controller: 'EncuentroController',
            data: {
                viewState: 'ENCUENTROS'
            }
        })
        .state('home.encuentro.altaEncuentro_2', {
            url: '/altaEncuentro',
            templateUrl: 'views/encuentro/alta_step_two.html',
            controller: 'EncuentroController',
            data: {
                viewState: 'ENCUENTROS'
            }
        })
        .state('home.encuentro.altaEncuentro_3', {
            url: '/altaEncuentro',
            templateUrl: 'views/encuentro/alta_step_three.html',
            controller: 'EncuentroController',
            data: {
                viewState: 'ENCUENTROS'
            }
        })
        .state('home.encuentro.detalleEncuentro', {
            url: '/detalle',
            templateUrl: 'views/encuentro/detalle_encuentro.html',
            controller: 'EncuentroController',
            data: {
                viewState: 'ENCUENTROS'
            }
        })
        .state('home.encuentro.listaEntradas', {
            url: '/lista-entradas',
            templateUrl: 'views/encuentro/entrada/lista_entradas.html',
            controller: 'AbmEntradaController',
            data: {
                viewState: 'ENTRADAS'
            }
        })
        .state('home.encuentro.altaEntrada', {
            url: '/alta-entrada',
            templateUrl: 'views/encuentro/entrada/alta_entradas.html',
            controller: 'AbmEntradaController',
            data: {
                viewState: 'ENTRADAS'
            }
        })
        .state('home.equipos', {
            url: '/equipos',
            controller: 'EquipoOrgController',
            templateUrl: 'views/delegaciones/equipo/lista_equipo.html',
            data: {
                viewState: 'EQUIPOS'
            }
        })
        .state('home.detalleEquipo', {
            url: '/detalleEquipo',
            templateUrl: 'views/delegaciones/equipo/detalle_equipo.html',
            controller: 'EquipoOrgController',
            data: {
                viewState: 'EQUIPOS'
            }
        })
        .state('home.listaDeportistas', {
            url: '/listaDeportistas',
            templateUrl: 'views/delegaciones/deportista/lista_deportista.html',
            controller: 'DeportistaOrgController',
            data: {
                viewState: 'EQUIPOS'
            }
        })
        .state('home.detalleDeportista', {
            url: '/detalleDeportista',
            templateUrl: 'views/delegaciones/deportista/detalle_deportista.html',
            controller: 'DeportistaOrgController',
            data: {
                viewState: 'EQUIPOS'
            }
        })
        .state('home.reporteVentas', {
            url: '/ventas_entradas',
            templateUrl: 'views/reportes/ventas_encuentro.html',
            controller: 'ReporteVentasController',
            data: {
                viewState: 'REPORTEVENTAS'
            }
        })
        /*MODULO DELEGACIONES*/
        .state('homeDelegacion', {
            url:'/delegacion',
            templateUrl: 'views/home/home_delegacion.html',
            controller: 'HomeDelegacionController',
            data: {
                roles: 'DELEGACION'
            },
            resolve : {
                getEventoDelegacion: function(delegacionService, navigationCache) {
                    delegacionService.getDetalleEventoDelegacion().then(
                        function success(data) {
                            navigationCache.setEventoDelegacion(data);
                        }
                    )
                }
            }
        })
        .state('homeDelegacion.editarPerfil',{
            url: '/perfil',
            templateUrl: 'views/delegaciones/editar_perfil.html',
            controller: 'PerfilController'
        })
        .state('homeDelegacion.listaEquipos', {
            url: '/listaEquipos',
            templateUrl: 'views/delegaciones/equipo/lista_equipo.html',
            controller: 'AbmEquipoController'
        })
        .state('homeDelegacion.altaEquipo', {
            url: '/altaEquipo',
            templateUrl: 'views/delegaciones/equipo/alta_equipo.html',
            controller: 'AbmEquipoController'
        })
        .state('homeDelegacion.detalleEquipo', {
            url: '/detalleEquipo',
            templateUrl: 'views/delegaciones/equipo/detalle_equipo.html',
            controller: 'AbmEquipoController'
        })
        .state('homeDelegacion.listaDeportistas', {
            url: '/listaDeportistas',
            templateUrl: 'views/delegaciones/deportista/lista_deportista.html',
            controller: 'AbmDeportistaController'
        })
        .state('homeDelegacion.detalleDeportista', {
            url: '/detalle-deportista',
            templateUrl: 'views/delegaciones/deportista/detalle_deportista.html',
            controller: 'AbmDeportistaController'
        })
        .state('homeDelegacion.altaDeportista', {
            url: '/altaDeportista',
            templateUrl: 'views/delegaciones/deportista/alta_deportista.html',
            controller: 'AbmDeportistaController'
        })
        .state('forbidden', {
            templateUrl : 'views/errors/forbidden.html',
            url: '/forbidden'
        })
        .state('404', {
            templateUrl: 'views/errors/404.html',
            url: '/404'
        })
        .state('index', {
            templateUrl: 'views/main.html',
            url: '/'
        })
        .state('entry', {
            templateUrl: 'views/main.html',
            url: ''
        });

    $urlRouterProvider.otherwise("404");

}]);

app.run(['$rootScope', '$state', 'authService', function ($rootScope, $state, authService) {
    $rootScope.$on('$stateChangeStart', function (event, toState, nextParams, fromState, fromParams) {
        //Aca segun los perfiles redirecciona a secciones donde no tenga permisos
        if(authService.isAuthenticated()) {
            if(toState.data && toState.data.roles) {
                if(!authService.authorize(toState.data.roles)) {
                    event.preventDefault();
                    $state.go('forbidden');
                }
            }
        }
        else {
            if(toState.name.indexOf('home') !== -1) {
                event.preventDefault();
                $state.go('index');
            }
        }
    });
}]);
/**
 * Init variables globales aplicacion
 */
app.run(['$rootScope', 'uiDatetimePickerConfig', '$state', 'authService', function($rootScope, uiDatetimePickerConfig, $state, authService){

    $rootScope.$on('SESSION_EXPIRED', function (event) {
        if(angular.isObject(event))
            event.preventDefault();
        authService.logout();
        $state.go('index');
        console.log('Sesión expirada.');
    });

    $rootScope.dateFormat = "dd/MM/yyyy";
    $rootScope.dateTimeFormat = "dd/MM/yyyy HH:mm a";
    $rootScope.altUserImagePath = 'styles/images/user.jpg';
    $rootScope.multiSelectLocalLang = {
        selectAll       : "Seleccionar Todos",
        selectNone      : "Quitar Todos",
        reset           : "Deshacer Todo",
        search          : "Buscar...",
        nothingSelected : "No hay elementos seleccionados..."
    };

    $rootScope.ICONS_PATH = 'styles/images/';

    uiDatetimePickerConfig.clearText = 'Borrar';
    uiDatetimePickerConfig.closeText = 'Cerrar';
    uiDatetimePickerConfig.dateText = 'Fecha';
    uiDatetimePickerConfig.nowText = 'Ahora';
    uiDatetimePickerConfig.timeText = 'Hora';
    uiDatetimePickerConfig.todayText = 'Hoy';

    var ssl = true;
    var SERVER_URL = (ssl ? 'https' : 'http') + '://services.ssem.org:8080/';
    $rootScope.sslConfig = ssl;
    $rootScope.SSEM_BACKEND_REST_DEF = 'ssem-backend/services/rest/';
    $rootScope.SSEM_BACKEND_COMMON = SERVER_URL + 'ssem-backend/services/rest/common/';
    $rootScope.SSEM_BACKEND_ENDPOINT = SERVER_URL + 'ssem-backend/services/rest/backoffice/';
    $rootScope.SSEM_BACKEND_SECURITY_ENDPOINT = SERVER_URL + 'ssem-backend/services/rest/security/';
}]);

app.config(function($provide){
    $provide.decorator('GridOptions',['$delegate', 'i18nService', function($delegate, i18nService){
        var gridOptions;
        gridOptions = angular.copy($delegate);
        gridOptions.initialize = function(options) {
            var initOptions;
            initOptions = $delegate.initialize(options);
            return initOptions;
        };
        i18nService.setCurrentLang('es');
        return gridOptions;
    }]);
});


//app.config(['uiGmapGoogleMapApiProvider', function(uiGmapGoogleMapApiProvider) {
//    uiGmapGoogleMapApiProvider.configure({
//        key: 'AIzaSyBLb75zUgktPm5YahPuKFFVvOj4jRGnlA8',
//        libraries: 'places,geometry,visualization'
//    });
//}]);
